from tkinter import ttk, messagebox, simpledialog
import math

from Model.Organisms.Animals.Antelope import Antelope
from Model.Organisms.Animals.CyberSheep import CyberSheep
from Model.Organisms.Animals.Fox import Fox
from Model.Organisms.Animals.Sheep import Sheep
from Model.Organisms.Animals.Turtle import Turtle
from Model.Organisms.Animals.Wolf import Wolf
from Model.Organisms.Plants.DeadlyNightshade import DeadlyNightshade
from Model.Organisms.Plants.Grass import Grass
from Model.Organisms.Plants.Guarana import Guarana
from Model.Organisms.Plants.SosnowskysHogweed import SosnowskysHogweed
from Model.Organisms.Plants.Thistle import Thistle
from Model.World import World
from tkinter import *
from Model.Organisms.Animals.Human import Human


def map_size_prompt_xy():
    size_x = simpledialog.askinteger("Virtual world size prompt", "Please enter board size x",
                                     minvalue=2, maxvalue=100)
    size_y = simpledialog.askinteger("Virtual world size prompt", "Please enter board size y",
                                     minvalue=2, maxvalue=100)
    return size_x,size_y


class MyCanvasDrawer:
    bg_color = 'beige'

    def __init__(self, drawed_on: Canvas, organism_size_xy: tuple, world_size_xy: tuple):
        self.__canvas = drawed_on
        self.organism_size = organism_size_xy
        self.__world_size_xy = world_size_xy
        self.__map = [[0 for _ in range(0, world_size_xy[1])] for _ in range(0, world_size_xy[0])]
        for y in range(0, world_size_xy[1]):
            for x in range(0, world_size_xy[0]):
                self.__map[x][y] = self.__canvas.create_rectangle(self.organism_size[0] * x,
                                                                  self.organism_size[1] * y,
                                                                  self.organism_size[0] * (x + 1),
                                                                  self.organism_size[1] * (y + 1),
                                                                  fill=self.bg_color)

    def represent_organism(self, organism):
        tile = self.__map[organism.position_xy[0]][organism.position_xy[1]]
        self.__canvas.itemconfig(tile, fill=organism.representation)

    def clean_canvas(self):
        for x in range(0, self.__world_size_xy[0]):
            for y in range(0, self.__world_size_xy[1]):
                self.__canvas.itemconfig(self.__map[x][y], fill=self.bg_color)


class GameView:
    __root: Tk

    map_panel_width = 600
    map_panel_height = 400
    map_panel_margin_vertical = 20
    map_panel_margin_horizontal = 40

    delayed_events = []

    def __init__(self, world: World, on_load_click, on_save_click):
        # Viewer stuff
        self.__root = Tk()
        self.__root.withdraw()
        self.__world = world
        self.__on_save_button_click = on_save_click
        self.__on_load_button_click = on_load_click

        self.__map_size_xy = (self.__world.size_x,self.__world.size_y)
        self.__organism_size_xy = self.map_panel_width / self.__map_size_xy[0], self.map_panel_height / \
                                  self.__map_size_xy[1]

        self.__root.deiconify()
        self.__root.resizable(width=0, height=0)
        self.__map_frame = Frame(self.__root,
                                 width=self.map_panel_width + self.map_panel_margin_horizontal,
                                 height=self.map_panel_height + self.map_panel_margin_vertical,
                                 bg="blue")
        self.__map_frame.pack(side=LEFT)
        self.__game_map = Canvas(self.__map_frame,
                                 width=self.map_panel_width,
                                 height=self.map_panel_height,
                                 bg="green")
        self.__game_map.pack()
        self.__game_map.bind("<Button-1>", self.on_tile_click)

        self.__navigation_frame = Frame(self.__root,
                                        width=self.map_panel_width + self.map_panel_margin_horizontal,
                                        height=self.map_panel_height + self.map_panel_margin_vertical,
                                        bg="red")

        self.__human_ability_label = Label(self.__navigation_frame, text="Not activated", fg="red")
        self.__human_ability_label.pack(side=TOP)

        self.__exit_button = Button(self.__navigation_frame, text="next turn", command=self.on_next_turn_button_click)
        self.__exit_button.pack(side=TOP)

        self.__load_button = Button(self.__navigation_frame, text="load game", command=self.__on_load_button_click)
        self.__load_button.pack(side=TOP)

        self.__save_button = Button(self.__navigation_frame, text="save game", command=self.__on_save_button_click)
        self.__save_button.pack(side=TOP)

        self.__navigation_frame.pack(side=RIGHT)

        self.__drawer = MyCanvasDrawer(self.__game_map, self.__organism_size_xy, self.__map_size_xy)

        # Data stuff
        self.__root.bind("<Left>", self.on_larrow_button_click)
        self.__root.bind("<Up>", self.on_uarrow_button_click)
        self.__root.bind("<Right>", self.on_rarrow_button_click)
        self.__root.bind("<Down>", self.on_darrow_button_click)
        self.__root.bind("a", self.on_human_ability_button_click)
        self.draw_organisms()

    def start_loop(self):
        self.__root.mainloop()

    def close(self):
        self.__root.destroy()

    def draw_organisms(self):
        if self.__world.is_human_alive():
            if self.__world.is_human_skill_active():
                self.change_human_ability_label((True,))
            else:
                self.change_human_ability_label((False,))
        else:
            self.change_human_ability_label((False,))

        for organism in self.__world.get_organisms():
            self.__drawer.represent_organism(organism)

    def next_round(self):
        self.__drawer.clean_canvas()
        self.__world.next_turn()
        self.draw_organisms()
        for delay_event_args in self.delayed_events:
            delay_event_args[0] -= 1
            if delay_event_args[0] == 0:
                delay_event_args[1](delay_event_args[2])
                self.delayed_events.remove(delay_event_args)

    def on_next_turn_button_click(self):
        print("Next round")
        if self.__world.is_human_alive():
            print("Please enter the direction for human using arrow keys")
            return
        self.next_round()

    def on_human_ability_button_click(self, event):
        if not self.__world.is_human_alive():
            messagebox.showwarning("Warning", "Human is not alive, cannot activate special ability")
            return
        if self.__world.activate_human_skill() is True:
            print("Human special ability activated")
            self.change_human_ability_label((True,))
            # self.delayed_events.append([5, self.change_human_ability_label, (False,)])
        else:
            messagebox.showwarning("Warning", "The ability is still on cooldown")

    def on_tile_click(self, event):
        tile_x = math.floor(event.x / self.map_panel_width * self.__map_size_xy[0])
        tile_y = math.floor(event.y / self.map_panel_height * self.__map_size_xy[1])

        if tile_x >= self.__world.size_x or tile_y >= self.__world.size_y:
            return
        if self.__world.is_occupied(tile_x,tile_y):
            return

        window = Toplevel()
        canvas = Canvas(window, width=200, height=200)
        canvas.pack(side=RIGHT)
        add_antelope_button = Button(window, text="Add antelope",
                                     command=lambda: self.add_organism_to_world(
                                         Antelope(self.__world, (tile_x, tile_y)),window))
        add_antelope_button.pack()

        add_sheep_button = Button(window, text="Add sheep",
                                  command=lambda: self.add_organism_to_world(Sheep(self.__world, (tile_x, tile_y)),window))
        add_sheep_button.pack()
        add_wolf_button = Button(window, text="Add wolf",
                                 command=lambda: self.add_organism_to_world(Wolf(self.__world, (tile_x, tile_y)),window))
        add_wolf_button.pack()
        add_fox_button = Button(window, text="Add fox",
                                command=lambda: self.add_organism_to_world(Fox(self.__world, (tile_x, tile_y)),window))
        add_fox_button.pack()
        add_turtle_button = Button(window, text="Add turtle",
                                   command=lambda: self.add_organism_to_world(Turtle(self.__world, (tile_x, tile_y)),window))
        add_turtle_button.pack()
        add_cybersheep_button = Button(window, text="Add cybersheep",
                                       command=lambda: self.add_organism_to_world(
                                           CyberSheep(self.__world, (tile_x, tile_y)),window))
        add_cybersheep_button.pack()
        add_grass_button = Button(window, text="Add grass",
                                  command=lambda: self.add_organism_to_world(Grass(self.__world, (tile_x, tile_y)),window))
        add_grass_button.pack()
        add_thistle_button = Button(window, text="Add thistle",
                                    command=lambda: self.add_organism_to_world(Thistle(self.__world, (tile_x, tile_y)),window))
        add_thistle_button.pack()
        add_guarana_button = Button(window, text="Add guarana",
                                    command=lambda: self.add_organism_to_world(Guarana(self.__world, (tile_x, tile_y)),window))
        add_guarana_button.pack()
        add_nightshade_button = Button(window, text="Add nightshade",
                                       command=lambda: self.add_organism_to_world(
                                           DeadlyNightshade(self.__world, (tile_x, tile_y)),window))
        add_nightshade_button.pack()
        add_hogweed_button = Button(window, text="Add hogweed",
                                    command=lambda: self.add_organism_to_world(
                                        SosnowskysHogweed(self.__world, (tile_x, tile_y)),window))
        add_hogweed_button.pack()

    def add_organism_to_world(self, organism, window = None):
        self.__world.add_organism(organism)
        self.draw_organisms()
        if window is not None:
            window.destroy()

    def change_human_ability_label(self, args: tuple):
        if args[0]:
            self.__human_ability_label['text'] = "Ability activated"
            self.__human_ability_label['fg'] = "green"
        else:
            self.__human_ability_label['text'] = "Ability not activated"
            self.__human_ability_label['fg'] = "red"

    def on_uarrow_button_click(self, event):
        if self.__world.is_human_alive():
            move = (0, -1)
            if Human.is_valid_position_given_change(self.__world.human, move):
                self.__world.human.move_direction_xy = move
                self.next_round()
            else:
                return

    def on_rarrow_button_click(self, event):
        if self.__world.is_human_alive():
            move = (1, 0)
            if Human.is_valid_position_given_change(self.__world.human, move):
                self.__world.human.move_direction_xy = move
                self.next_round()
            else:
                return

    def on_larrow_button_click(self, event):
        if self.__world.is_human_alive():
            move = (-1, 0)
            if Human.is_valid_position_given_change(self.__world.human, move):
                self.__world.human.move_direction_xy = move
                self.next_round()
            else:
                return

    def on_darrow_button_click(self, event):
        if self.__world.is_human_alive():
            move = (0, 1)
            if Human.is_valid_position_given_change(self.__world.human, move):
                self.__world.human.move_direction_xy = move
                self.next_round()
            else:
                return
