import pickle

from Model.Organisms.Animals.Human import Human
from Model.World import World
from View.GameView import GameView, map_size_prompt_xy, Tk


class Projector:
    def __init__(self):
        temp_root = Tk()
        temp_root.withdraw()
        world_size_xy = map_size_prompt_xy()
        self.__world = World(world_size_xy[0],world_size_xy[1])
        temp_root.destroy()

        self.__world.add_organism(Human(self.__world, (2, 4)))
        self.__world_view = GameView(self.__world, self.load_game,self.save_game)


    def start_game(self):
        self.__world_view.start_loop()

    def save_game(self):
        with open("save.save", mode='wb') as pickle_file:
            pickle.dump(self.__world, pickle_file, protocol=-1)

    def load_game(self):
        with open("save.save", mode='rb') as pickle_file:
            self.__world = pickle.load(pickle_file)
            self.__world_view.close()
            self.__world_view = GameView(self.__world, self.load_game, self.save_game)
        self.start_game()

