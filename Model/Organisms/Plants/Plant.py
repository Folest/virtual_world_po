import random
from ..Organism import Organism, CollisionResult
from abc import ABC, abstractmethod


class Plant(Organism):
    def reproduce(self, child_position_xy: tuple):
        pass

    def collision(self, offender):
        return CollisionResult.DEFENDER_KILLED

    def action(self):
        spreaded = random.randrange(0, 101)
        if spreaded < 6:
            child_position_xy = self.world.random_unoccupied_congruent_field_xy(self.position_xy)
            if child_position_xy is not None:
                self.reproduce(child_position_xy)

    @abstractmethod
    def defensive_ability(self, other):
        return super().defensive_ability(other)

    @abstractmethod
    def offensive_ability(self, other):
        return super().defensive_ability(other)