from .Plant import Plant, Organism, CollisionResult
from ..Animals.Animal import Animal, Organism


class SosnowskysHogweed(Plant):
    # Theres gotta be a better way...
    currently_alive = []
    currently_alive.insert(0, 0)

    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.strength = 10
        self.initiative = 0
        self.representation = "black"
        self.currently_alive[0] += 1
        print("There are now {} sosnowskis hogweeds".format(self.currently_alive))

    def collision(self, offender):
        return CollisionResult.OFFENDER_KILLED

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(SosnowskysHogweed(self.world, child_position_xy))

    def defensive_ability(self, other: Organism):
        return False

    def offensive_ability(self, other):
        return False

    def action(self):
        super().action()
        for dy in range(-1, 2):
            for dx in range(-1, 2):
                if not (dx == 0 and dy == 0) and super().is_valid_position_given_change((dx, dy)):
                    current_field = self.position_xy[0] + dx, self.position_xy[1] + dy
                    current_field_organism = self.world.get_organism_at_field(current_field)
                    if current_field_organism is not None and type(current_field_organism) is not type(self):
                        if not current_field_organism.defensive_ability(self):
                            print("A hogweed just killed organism of type{}".format(
                                type(current_field_organism).__name__))
                            current_field_organism.die()

    def die(self):
        self.currently_alive[0] -= 1
        print("A sosnowski has just died")
        super().die()
