from .Plant import Plant


class Thistle(Plant):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.strength = 0
        self.initiative = 0
        self.representation = "yellow"

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Thistle(self.world, child_position_xy))

    def defensive_ability(self, other):
        return False

    def offensive_ability(self, other):
        return False

    def action(self):
        for _ in range(0, 3):
            super().action()
