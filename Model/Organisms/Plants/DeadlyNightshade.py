from .Plant import Plant, Organism, CollisionResult


class DeadlyNightshade(Plant):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.strength = 99
        self.initiative = 0
        self.representation = "red"

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(DeadlyNightshade(self.world, child_position_xy))

    def defensive_ability(self, other: Organism):
        return False

    def offensive_ability(self, other):
        return False

    def action(self):
        super().action()

    def collision(self, offender: Organism):
        # if not offender.defensive_ability(self):
        return CollisionResult.OFFENDER_KILLED
        # return CollisionResult.DEFENDER_BLOCKED_ATTACK





