from .Plant import Plant


class Grass(Plant):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.strength = 0
        self.initiative =0
        self.representation = "lawngreen"

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Grass(self.world, child_position_xy))

    def defensive_ability(self, other):
        return False

    def offensive_ability(self, other):
        return False







