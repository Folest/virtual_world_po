from .Plant import Plant, Organism


class Guarana(Plant):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.strength = 0
        self.initiative = 0
        self.representation = "red"

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Guarana(self.world, child_position_xy))

    def defensive_ability(self, other: Organism):
        other.strength = other.strength + 3

    def offensive_ability(self, other):
        pass

    def action(self):
        super().action()
