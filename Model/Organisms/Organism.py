from abc import ABC, abstractmethod
from enum import Enum
import random


class CollisionResult(Enum):
    OFFENDER_KILLED = 0,
    OFFENDER_ESCAPED = 1,
    DEFENDER_KILLED = 2,
    REPRODUCTION = 3,
    DEFENDER_ESCAPED = 4,
    DEFENDER_BLOCKED_ATTACK = 5



class Organism(ABC):
    def __init__(self, world, position: tuple):
        self.world = world
        self._position_xy = position
        self._initiative = -1
        self._strength = -1
        self._representation = "blue"

    @abstractmethod
    def action(self):
        pass

    @abstractmethod
    def collision(self, offender):
        pass

    @abstractmethod
    def reproduce(self, child_position_xy: tuple):
        pass

    @property
    def position_xy(self):
        """First element in returned tuple is x position of organisms second is y"""
        return self._position_xy

    @property
    def initiative(self):
        return self._initiative

    @initiative.setter
    def initiative(self, initiative):
        self._initiative = initiative


    @position_xy.setter
    def position_xy(self, new_position: tuple):
        if self.is_valid_position(new_position) or new_position == (-1,-1):
            self._position_xy = new_position

    @property
    def representation(self):
        return self._representation

    @representation.setter
    def representation(self, color):
        self._representation = color

    @property
    def strength(self):
        return self._strength

    @strength.setter
    def strength(self, new_value):
        self._strength = new_value


    def defensive_ability(self, other):
        return False

    def offensive_ability(self, other):
        return False

    def die(self):
        self.world.remove_organism(self)
        self.position_xy = (-1, -1)
        self.world.move_to_graveyard(self)

    # Utility methods

    def is_valid_position(self, position_xy: tuple):
        # noinspection PyChainedComparisons
        if (position_xy[0] >= 0 and position_xy[1] >= 0 and
                position_xy[0] < self.world.size_x and
                position_xy[1] < self.world.size_y):
            return True
        else:
            return False

    def is_valid_position_given_change(self, position_change_xy: tuple):
        if self.is_valid_position(
                (self.position_xy[0] + position_change_xy[0], self.position_xy[1] + position_change_xy[1])):
            return True
        return False

    def random_congruent_field(self):
        dx = random.randrange(-1, 2)
        dy = random.randrange(-1, 2)
        while dx == 0 and dy == 0 or not self.is_valid_position_given_change((dx, dy)):
            dx = random.randrange(-1, 2)
            dy = random.randrange(-1, 2)
        return self.position_xy[0] + dx, self.position_xy[1] + dy
