from Model.Organisms.Organism import Organism,CollisionResult
from .Animal import Animal


class Sheep(Animal):
    color = "white"
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "white"
        self.strength = 4
        self.initiative = 4

    def offensive_ability(self, other):
        super().offensive_ability(other)

    def defensive_ability(self, other):
        super().defensive_ability(other)

    def action(self):
        super().action()

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Sheep(self.world, child_position_xy))

    def collision(self, offender: Organism):
        return super().collision(offender)