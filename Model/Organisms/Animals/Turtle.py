import random

from Model.Organisms.Organism import Organism
from .Animal import Animal


class Turtle(Animal):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "green"
        self.initiative = 1
        self.strength = 2

    def offensive_ability(self, other):
        return super().offensive_ability(other)

    def defensive_ability(self, other: Organism):
        if other.strength < 5:
            print("A turtle just blocked organism of type {} attack".format(type(other).__name__))
            return True
        return False

    def action(self):
        chance = random.randrange(0, 4)
        if chance <= 2:
            return
        super().action()

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Turtle(self.world, child_position_xy))

    def collision(self, offender: Organism):
        return super().collision(offender)
