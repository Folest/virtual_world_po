import random

from Model.Organisms.Organism import Organism
from .Animal import Animal, CollisionResult


class Antelope(Animal):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "salmon4"
        self.strength = 4
        self.initiative = 4

    def offensive_ability(self, other):
        super().offensive_ability(other)

    def random_congruent_field(self):
        dx = random.randrange(-1, 2)
        dy = random.randrange(-1, 2)
        while dx == 0 and dy == 0 or not self.is_valid_position_given_change((dx, dy)):
            dx = random.randrange(-1, 2)
            dy = random.randrange(-1, 2)
            double_dy_or_dx = random.randrange(0, 2)
            if double_dy_or_dx == 0 and dx > 0:
                dx = dx * 2
            else:
                dx = dx * 2
        if self.position_xy[0] + dx >= self.world.size_x or self.position_xy[1] + dy >= self.world.size_y:
            raise Exception("?????")
        return self.position_xy[0] + dx, self.position_xy[1] + dy

    def defensive_ability(self, other):
        super().defensive_ability(other)

    def action(self):
        new_position = self.random_congruent_field()
        if self.world.is_occupied(new_position[0], new_position[1]):
            escaped_from_fight = random.randrange(0, 2)
            if escaped_from_fight == 1:
                self.action()
                return
            result = self.world.get_organism_at_field(new_position).collision(self)
            self.handle_collision_result(result, new_position)
        else:
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Antelope(self.world, child_position_xy))

    def collision(self, offender: Organism):
        escaped_from_fight = random.randrange(0, 2)
        if escaped_from_fight == 0:
            super().action()
        else:
            return CollisionResult.DEFENDER_ESCAPED
