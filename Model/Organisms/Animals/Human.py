import random
from typing import List, Any

from Model.Organisms.Organism import Organism, CollisionResult
from .Animal import Animal


class Human(Animal):
    
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "magenta"
        self.initiative = 4
        self.strength = 5
        self.move_direction_xy = (0, 0)
        self.ability_cooldown = 0
        world.human = self

    def offensive_ability(self, other):
        return False

    def defensive_ability(self, other: Organism):
        if self.ability_enabled:
            return True
        return False

    def action(self):
        if not self.ability_available:
            self.ability_cooldown -= 1
        super().action()

    @property
    def ability_available(self):
        if self._ability_cooldown == 0:
            return True
        return False

    @property
    def ability_enabled(self):
        if self._ability_cooldown >= 5:
            return True
        return False

    @property
    def ability_cooldown(self):
        return self._ability_cooldown

    @ability_cooldown.setter
    def ability_cooldown(self, value: int):
        self._ability_cooldown = value

    def enable_ability(self):
        if self.ability_available:
            self.ability_cooldown = 10

    def reproduce(self, child_position_xy: tuple):
        pass

    def die(self):
        if self.ability_enabled:
            if self.escape():
                return
        self.world.human = None
        super().die()

    def escape(self)->bool:
        if self.ability_enabled:
            escape_to = self.world.random_unoccupied_congruent_field_xy(self.position_xy)
            if escape_to is not None:
                self.world.move_organism(self.position_xy, escape_to)
                self.position_xy = escape_to
                return True
        return False

    def random_congruent_field(self):
        return self.move_direction_xy

    @property
    def move_direction_xy(self):
        return self._move_direction_xy

    @move_direction_xy.setter
    def move_direction_xy(self, direction_xy: tuple):
        self._move_direction_xy = self.position_xy[0] + direction_xy[0], self.position_xy[1] + direction_xy[1]

    def collision(self, offender: Organism):
        return super().collision(offender)

    # def collision(self, offender: Organism):
    #     """Invoked for attacked organism (defender)"""
    #     if offender.strength >= self.strength:
    #         if not self.defensive_ability(offender):
    #             print("A organism of type {} just killed org of type: {}".format(type(offender).__name__,
    #                                                                              type(self).__name__))
    #             return CollisionResult.DEFENDER_KILLED
    #         else:
    #             return CollisionResult.DEFENDER_ESCAPED
    #     if offender.strength < self.strength:
    #         print("A organism of type {} just attacked org of type: {} and died".format(type(offender).__name__,
    #                                                                                     type(self).__name__))
    #         return CollisionResult.OFFENDER_KILLED

    # def handle_collision_result(self, result: CollisionResult, new_position: tuple):
    #     if result == CollisionResult.DEFENDER_KILLED:
    #         defender = self.world.get_organism_at_field(new_position)
    #         defender.die()
    #         self.world.move_organism(self.position_xy, new_position)
    #         self.position_xy = new_position
    #         return
    #     if result == CollisionResult.OFFENDER_KILLED:
    #
    #         self.die()
    #         return
    #     if result == CollisionResult.DEFENDER_BLOCKED_ATTACK:
    #         return
    #     if result == CollisionResult.DEFENDER_ESCAPED:
    #         self.world.move_organism(self.position_xy, new_position)
    #         self.position_xy = new_position
    #     else:
    #         raise Exception("Unhandled result of collision")
