from Model.Organisms.Organism import Organism
from .Animal import Animal


class Fox(Animal):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "orange"
        self.initiative = 7
        self.strength = 3

    def offensive_ability(self, other):
        super().offensive_ability(other)

    def defensive_ability(self, other):
        super().defensive_ability(other)

    def action(self):
        new_position = self.random_congruent_field()
        if new_position[0] >= self.world.size_x or new_position[1] >= self.world.size_y:
            raise Exception
        if self.world.is_occupied(new_position[0], new_position[1]):
            if self.world.get_organism_at_field(new_position).strength > self.strength:
                return
            result = self.world.get_organism_at_field(new_position).collision(self)
            self.handle_collision_result(result, new_position)
        else:
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Fox(self.world, child_position_xy))

    def collision(self, offender: Organism):
        return super().collision(offender)
