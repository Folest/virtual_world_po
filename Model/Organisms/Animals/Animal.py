from ..Organism import Organism, CollisionResult
from abc import ABC, abstractmethod


class Animal(Organism, ABC):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)

    @abstractmethod
    def collision(self, offender: Organism):
        """Invoked for attacked organism (defender)"""
        if isinstance(offender, type(self)):
            return CollisionResult.REPRODUCTION
        if offender.strength >= self.strength:
            if not self.defensive_ability(offender):
                print("A organism of type {} just killed org of type: {}".format(type(offender).__name__,
                                                                                 type(self).__name__))
                return CollisionResult.DEFENDER_KILLED
            else:
                return CollisionResult.DEFENDER_BLOCKED_ATTACK
        if offender.strength < self.strength:
            print("A organism of type {} just attacked org of type: {} and died".format(type(offender).__name__,
                                                                                        type(self).__name__))
            return CollisionResult.OFFENDER_KILLED

    @abstractmethod
    def reproduce(self, child_position_xy: tuple):
        pass

    @abstractmethod
    def action(self):
        new_position = self.random_congruent_field()
        defender = self.world.get_organism_at_field((new_position[0], new_position[1]))
        if defender is not None:
            result = defender.collision(self)
            self.handle_collision_result(result, new_position)
        else:
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position

    @abstractmethod
    def defensive_ability(self, other):
        return super().defensive_ability(other)

    @abstractmethod
    def offensive_ability(self, other):
        return super().defensive_ability(other)

    def handle_collision_result(self, result: CollisionResult, new_position: tuple):
        if result == CollisionResult.DEFENDER_KILLED:
            defender = self.world.get_organism_at_field(new_position)
            defender.die()
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position
            return
        if result == CollisionResult.OFFENDER_KILLED:
            self.die()
            return
        if result == CollisionResult.REPRODUCTION:
            unoccupied_field_xy = self.world.random_unoccupied_congruent_field_xy(self.position_xy)
            if unoccupied_field_xy is not None:
                self.reproduce(unoccupied_field_xy)
            return
        if result == CollisionResult.DEFENDER_BLOCKED_ATTACK:
            return
        if result == CollisionResult.DEFENDER_ESCAPED:
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position
