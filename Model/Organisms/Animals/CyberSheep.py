from Model.Organisms.Organism import Organism, CollisionResult
from .Sheep import Sheep
from ..Plants.SosnowskysHogweed import SosnowskysHogweed


class CyberSheep(Sheep):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "blue"
        self.initiative = 4
        self.strength = 11

    @property
    def representation(self):
        if SosnowskysHogweed.currently_alive[0] > 0:
            return self._representation
        else:
            return Sheep.color

    @representation.setter
    def representation(self, value):
        self._representation = value

    @property
    def strength(self):
        if SosnowskysHogweed.currently_alive[0] > 0:
            return self._strength
        else:
            return self._strength - 7

    @strength.setter
    def strength(self, value):
        self._strength = value

    def offensive_ability(self, other):
        super().offensive_ability(other)

    def defensive_ability(self, other):
        if isinstance(other, SosnowskysHogweed):
            return True
        return False

    def action(self):
        if SosnowskysHogweed.currently_alive[0] > 0:
            new_position = self.go_to_closest_hogweed(self.find_closest_hogweed())
        else:
            new_position = self.random_congruent_field()
        if self.world.get_organism_at_field(new_position) is not None:
            new_position_organism = self.world.get_organism_at_field(new_position)
            if isinstance(new_position_organism, SosnowskysHogweed):
                result = CollisionResult.DEFENDER_KILLED
            else:
                result = self.world.get_organism_at_field(new_position).collision(self)
            self.handle_collision_result(result, new_position)
        else:
            self.world.move_organism(self.position_xy, new_position)
            self.position_xy = new_position

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(CyberSheep(self.world, child_position_xy))

    def collision(self, offender: Organism):
        return super().collision(offender)

    def find_closest_hogweed(self):
        organisms = self.world.get_organisms()
        # setting it to bigger value than biggest distance possible on current map
        smallest_distance = self.world.size_x ** 2 + self.world.size_y ** 2
        closest_hogweed = None
        for organism in organisms:
            if isinstance(organism, SosnowskysHogweed) and organism.position_xy != (-1, -1):
                distance_x = abs(self.position_xy[0] - organism.position_xy[0])
                distance_y = abs(self.position_xy[1] - organism.position_xy[1])
                distance = pow(distance_x ** 2 + distance_y ** 2, 0.5)
                if distance < smallest_distance:
                    closest_hogweed = organism
        print("The closest hogweed is at x: {} y: {}"
              "".format(closest_hogweed.position_xy[0], closest_hogweed.position_xy[1]))
        return closest_hogweed

    def go_to_closest_hogweed(self, closest_hogweed: SosnowskysHogweed):
        if closest_hogweed.position_xy[0] > self.position_xy[0]:
            dx = 1
        elif closest_hogweed.position_xy[0] == self.position_xy[0]:
            dx = 0
        else:
            dx = -1
        if closest_hogweed.position_xy[1] > self.position_xy[1]:
            dy = 1
        elif closest_hogweed.position_xy[1] == self.position_xy[1]:
            dy = 0
        else:
            dy = -1
        return self.position_xy[0] + dx, self.position_xy[1] + dy
