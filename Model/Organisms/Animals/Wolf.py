from Model.Organisms.Organism import Organism
from .Animal import Animal


class Wolf(Animal):
    def __init__(self, world, position: tuple):
        super().__init__(world, position)
        self.representation = "gray"
        self.initiative = 5
        self.strength = 9

    def offensive_ability(self, other):
        super().offensive_ability(other)

    def defensive_ability(self, other):
        super().defensive_ability(other)

    def action(self):
        super().action()

    def reproduce(self, child_position_xy: tuple):
        self.world.add_organism(Wolf(self.world, child_position_xy))

    def collision(self, offender: Organism):
        return super().collision(offender)
