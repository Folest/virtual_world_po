import random

from Model.Organisms.Animals.Sheep import Sheep
from Model.Organisms import Organism
from Model.Organisms.Animals.Human import Human


class World:
    def __init__(self, size_x: int, size_y: int):
        self.size_x = size_x
        self.size_y = size_y
        self.__organisms = []
        self.__graveyard = []
        self.__game_map_occupation = [[False for x in range(size_x)] for y in range(size_y)]
        self.human = None

    def next_turn(self):
        self.__organisms.sort(key=lambda org: -org.initiative)
        for organism in self.__organisms:
            if not organism.position_xy == (-1, -1):
                organism.action()
        self.clean_graveyard()

    def is_occupied(self, x: int, y: int):
        return self.__game_map_occupation[x][y]

    def get_organism_at_field(self, organism_xy: tuple) -> Organism:
        for organism in self.__organisms:
            if organism.position_xy[0] == organism_xy[0] and organism.position_xy[1] == organism_xy[1]:
                return organism
        return None

    def get_organisms(self):
        return self.__organisms

    def move_organism(self, old_position_xy: tuple, new_position_xy: tuple):
        # DEBUG
        self.__game_map_occupation[old_position_xy[0]][old_position_xy[1]] = False
        self.__game_map_occupation[new_position_xy[0]][new_position_xy[1]] = True

    def move_to_graveyard(self, organism):
        self.__graveyard.append(organism)

    def clean_graveyard(self):
        for dead in self.__graveyard:
            self.__organisms.remove(dead)
        self.__graveyard.clear()

    def add_organism(self, added_organism: Organism):
        x = added_organism.position_xy[0]
        y = added_organism.position_xy[1]
        # if self.__game_map_occupation[x][y] or self.get_organism_at_field(added_organism.position_xy):
            # raise Exception("Duplication of organism on a field")
        # else:
        self.__organisms.append(added_organism)
        self.__game_map_occupation[x][y] = True

    def remove_organism(self, removed: Organism):
        removed_x = removed.position_xy[0]
        removed_y = removed.position_xy[1]
        if not self.__game_map_occupation[removed_x][removed_y]:
            raise Exception
        self.__game_map_occupation[removed_x][removed_y] = False

        @property
        def human(self):
            return

        @human.setter
        def human(self, human_to_be_set):
            self._human = human_to_be_set

    # Utility methods
    def random_unoccupied_congruent_field_xy(self, congruent_to_xy: tuple):
        x = congruent_to_xy[0]
        y = congruent_to_xy[1]
        unoccupied_fields_xy = []
        for dy in range(-1, 2):
            for dx in range(-1, 2):
                if self.is_valid_position((x + dx, y + dy)) and not self.__game_map_occupation[x + dx][y + dy]:
                    unoccupied_fields_xy.append((x + dx, y + dy))
        if len(unoccupied_fields_xy) > 0:
            choice = random.randrange(0, len(unoccupied_fields_xy))
            return unoccupied_fields_xy[choice]
        return None

    def is_valid_position(self, position_xy: tuple):
        # noinspection PyChainedComparisons
        if (position_xy[0] >= 0 and position_xy[1] >= 0 and
                position_xy[0] < self.size_x and
                position_xy[1] < self.size_y):
            return True
        else:
            return False

    def is_human_alive(self) -> bool:
        if self.human is not None:
            return True
        return False

    def is_human_skill_active(self) -> bool:
        if self.human.ability_enabled:
            return True
        return False

    def activate_human_skill(self) -> bool:
        if self.is_human_alive():
            if self.human.ability_available:
                self.human.enable_ability()
                return True
        return False
